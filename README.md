# 18.06
Code solutions for problems in 18.06
[Course homepage](https://github.com/mitmath/1806/blob/master/summaries.md) 

Introduction to Linear Algebra (5th Ed)
Solutions can be found [here](https://math.mit.edu/~gs/linearalgebra/) 
 
 Code in MATLAB and Julia


